var numa;
var numb;
var operacion;

function inicial(){
    var pantalla = document.getElementById('pantalla');
    var reset = document.getElementById('reset')
    var restar = document.getElementById('resta');
    var sumar = document.getElementById('suma');
    var igual = document.getElementById('igual');
    var uno = document.getElementById('uno');
    var dos = document.getElementById('dos');
    var tres = document.getElementById('tres');
    var cuatro = document.getElementById('cuatro');
    var cinco = document.getElementById('cinco');
    var seis = document.getElementById('seis');
    var siete = document.getElementById('siete');
    var ocho = document.getElementById('ocho');
    var nueve = document.getElementById('nueve');
    var cero = document.getElementById('cerot');

    uno.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "1";
    } 
    dos.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "2";
    } 
    tres.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "3";
    } 
    cuatro.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "4";
    } 
    cinco.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "5";
    } 
    seis.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "6";
    } 
    siete.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "7";
    } 
    ocho.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "8";
    } 
    nueve.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "9";
    } 
    cero.onclick = function(e){
        pantalla.textContent = pantalla.textContent  + "0";
    } 
    reset.onclick = function(e){
        resetear();
    }

    sumar.onclick = function(e){
        numa=pantalla.textContent;
        operacion = "+";
        limpiar();
        
    }
    restar.onclick = function(e){
        numa=pantalla.textContent;
        operacion = "-";
        limpiar();
    }
    
    igual.onclick = function(e){
        numb=pantalla.textContent;
        resolver(); 
    }

    function limpiar(){
        pantalla.textContent= " ";
    }
    
    function resetear (){
        pantalla.textContent = " ";
        numa = 0;
        numb = 0;
        operacion = " ";
    }
}

function resolver(){
    var res = 0;
    switch(operacion){
        case "+":
            res = parseFloat(numa) + parseFloat(numb);
            break;
        case "-":
            res = parseFloat(numa) - parseFloat(numb);
            break;
    }
    pantalla.textContent = res;
}
